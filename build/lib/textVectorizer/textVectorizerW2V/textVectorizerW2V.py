import os
import numpy as np
import json

import gensim

from textPreprocessor import TextPreprocessor

from tensorflow.python.keras.preprocessing.sequence import pad_sequences
#from core.supportFunctions.padSequence import padSequence

from ..textVectorizer import TextVectorizer


class TextVectorizerW2V(TextVectorizer):
    def __init__(self):
        self.model_ = None
        self.textPreprocessor_ = None

        self.vectorizeLemmas_ = True

    def __del__(self):
        del self.textPreprocessor_
        del self.model_

    def vectorize(self, text):
        wordVectors = []
        tokens = self.textPreprocessor_.getFilteredTokens(text, self.modelLang_)
        if self.vectorizeLemmas_ == True:
            for t in tokens:
                vector = self.convertWordToVector(t.lemma_)
                if vector is not None:
                    wordVectors.append(vector)
        else:
            for t in tokens:
                vector = self.convertWordToVector(t.text)
                if vector is not None:
                    wordVectors.append(vector)

        if self.padSize_ is not None:
            wordVectorWithAdditionalDimention = np.array([wordVectors], dtype=self.dtype_)  # CRUTCH!
            paddedWordVectorWithAdditionalDimention = pad_sequences(wordVectorWithAdditionalDimention,
                                                                    self.padSize_,
                                                                    truncating='post',
                                                                    dtype=self.dtype_)
            del wordVectorWithAdditionalDimention
            del wordVectors
            paddedWordVectors = paddedWordVectorWithAdditionalDimention[0]
            del paddedWordVectorWithAdditionalDimention
            return paddedWordVectors
        else:
            return np.array(wordVectors, dtype=self.dtype_)

    def vectorizeSequence(self, textSequence):
        textVectorSequence = []
        for text in textSequence:
            wordVectors = []
            tokens = self.textPreprocessor_.getFilteredTokens(text, self.modelLang_)
            if self.vectorizeLemmas_ == True:
                for t in tokens:
                    vector = self.convertWordToVector(t.lemma_)
                    if vector is not None:
                        wordVectors.append(vector)
            else:
                for t in tokens:
                    vector = self.convertWordToVector(t.text)
                    if vector is not None:
                        wordVectors.append(vector)
            textVectorSequence.append(np.array(wordVectors, dtype=self.dtype_))

        if self.padSize_ is not None:
            return pad_sequences(np.array(textVectorSequence, dtype=self.dtype_),
                                 self.padSize_,
                                 truncating='post',
                                 dtype=self.dtype_)
        else:
            return np.array(textVectorSequence, dtype=self.dtype_)

    def vectorizeWordSequence(self, wordSequence):
        wordVectors = []
        for word in wordSequence:
            vector = self.convertWordToVector(word)
            if vector is not None:
                wordVectors.append(vector)

        if len(wordVectors) == 0:
            return None

        if self.padSize_ is not None:
            wordVectorWithAdditionalDimention = np.array([wordVectors], dtype=self.dtype_)  # CRUTCH!
            paddedWordVectorWithAdditionalDimention = pad_sequences(wordVectorWithAdditionalDimention,
                                                                    self.padSize_,
                                                                    truncating='post',
                                                                    dtype=self.dtype_)
            del wordVectorWithAdditionalDimention
            del wordVectors
            paddedWordVectors = paddedWordVectorWithAdditionalDimention[0]
            del paddedWordVectorWithAdditionalDimention
            return paddedWordVectors
        else:
            return np.array(wordVectors, dtype=self.dtype_)

    def convertWordToVector(self, word, defaultResult=None):
        try:
            res = self.model_.wv[word]
            return res
        except:
            return defaultResult


# SETTERS AND GETTERS:

    def setModel(self, modelPath, modelType, modelLang):
        del self.model_
        self.modelPath_ = modelPath
        self.modelType_ = modelType
        self.modelLang_ = modelLang
        if modelType == "fasttext":
            # https://radimrehurek.com/gensim/models/keyedvectors.html
            # https://radimrehurek.com/gensim/models/fasttext.html#gensim.models.fasttext.FastText
            self.model_ = gensim.models.FastText.load_fasttext_format(self.modelPath_, full_model=False)
        else:
            self.model_ = gensim.models.KeyedVectors.load(self.modelPath_, binary=True)
    def getModel(self):
        return self.model_

    def setTextPreprocessor(self, preproc):
        del self.textPreprocessor_
        self.textPreprocessor_ = preproc
    def getTextPreprocessor(self):
        return self.textPreprocessor_

    def setPadSize(self, padSize):
        self.padSize_ = padSize
    def getPadSize(self):
        return self.padSize_

    def setVectorizeLemmas_(self, vectorizeLemmas):
        self.vectorizeLemmas_ = vectorizeLemmas
    def getVectorizeLemmas_(self):
        return self.vectorizeLemmas_

# SAVE AND LOAD:

    def saveThisObjectDataOnly_(self, folder):
        self.textPreprocessor_.save(os.path.join(folder, "textPreprocessor"))
        with open(os.path.join(folder, "parameters.json"), "w") as fp:
            parameters = {}
            parameters["vectorizeLemmas"] = self.vectorizeLemmas_

            parameters["modelPath"] = self.modelPath_
            parameters["modelType"] = self.modelType_
            parameters["modelLang"] = self.modelLang_

            parameters["padSize"] = self.padSize_
            json.dump(parameters, fp)

    def loadThisObjectDataOnly_(self, folder):
        parameters = None
        with open(os.path.join(folder, "parameters.json"), "r") as fp:
            parameters = json.load(fp)
        self.vectorizeLemmas_ = parameters["vectorizeLemmas"]
        self.padSize_ = parameters["padSize"]

        self.setModel(parameters["modelPath"], parameters["modelType"], parameters["modelLang"])

        self.textPreprocessor_ = TextPreprocessor.load(os.path.join(folder, "textPreprocessor"))

# FIELDS:

    textPreprocessor_ = None
    vectorizeLemmas_ = None

    modelPath_ = None
    modelType_ = None    # wordtovec or fasttext
    modelLang_ = None
    model_ = None

    padSize_ = None


TextVectorizerW2V.registerClass("TextVectorizerW2V")