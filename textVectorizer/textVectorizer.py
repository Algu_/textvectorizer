import numpy as np

from serializableClass import SerializableClass

class TextVectorizer(SerializableClass):
    def vectorize(self, text):
        pass

    def vectorizeSequence(self, textSequence):
        pass

# FIELDS:

    dtype_ = np.float32

TextVectorizer.initNewRootOfInheritance()