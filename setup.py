from setuptools import setup, find_packages
from os.path import join, dirname

from textVectorizer import __version__

import textPreprocessor

setup(
    name='textVectorizer',
    description='This project is dedicated to conversion of text to vector form.',
    long_description=open(join(dirname(__file__), 'README.md')).read(),
	classifiers=['Programming Language :: Python :: 3.6', 'Topic :: Text Processing :: Linguistic',],
    version=__version__,
    url='https://bitbucket.org/Algu_/textvectorizer/src/master/',
    author='Alexander Gusarin',
    author_email='alex.gusarin@gmail.com',
    license='MIT',
    packages=find_packages(),
	install_requires=['Cython', 'textPreprocessor', 'Gensim'],
)